#!/usr/bin/env python2.7
import subprocess
import Tkinter as tk
import ttk
from functools import partial
from json import dumps
from threading import Thread, Lock
from time import sleep

"""
Command line tool                  Description
rosversion <package/stack>         print package/stack version
rosversion -d                      print ROS version

rosmsg list                        list all available ROS messages
rosmsg show <message>              (== rosmsg info <message>) print message format
rosmsg package <package>           list all messages from package
rosmsg packages                    list all packages that have at least one message
rosmsg md5 <message>               print MD5 checksum for message
"""

"""
@todo Add search entry for a given package (use autocomplete) with auto-focus and auto-select feature. If a dependency is entered, all packages that have it will be marked and the first (topmost) will be scrolled to
@todo Upgrade the way the treeview gets populated:
      1) Get all available packages
      2) Add custom progressbar (tk not ttk) that shows the current status of resolving the dependencies of the packages (it also shows number of processed packages out of total numer - like my PyQt apps)
      3) Call insert() for treeview after each resolved package and update the progressbar
      4) While resolving package dependencies block all actions with the treeview (set state to "disabled" possible? Does it affect scrolling?) except for scrolling and resizing the columns
      5) Remove progressbar when it reaches 100% (https://stackoverflow.com/a/3819568/1559401)
@todo Move populate() to a separate thread to allow loading packages AND updating the UI at the same time. self.loading_process needs to be locked with a mutex
@todo Fixing issues would simply replace the incorrect information in the given entries without deleting those. No need to reload the complete table!
"""

class RosPackageView(ttk.Frame):
    
    class PackageRetrieval(Thread):
        def __init__(self, progress_var):
            Thread.__init__(self)
            self.mutex = Lock()
            
        def run(self):
            self.mutex.acquire()
            while True:
                print("Thread working...")
                #progress_var += 1
                sleep(1)
            self.mutex.release()
    
    def __init__(self, master):
        ttk.Frame.__init__(self, master)
        
        self.f_packages = ttk.Frame(self)
        
        self.__setup_treeview__()
        self.tv_packages.grid(row=0, column=0, sticky="nwes")
        self.__setup_contextmenu_()
        self.f_packages.grid(sticky="nwes")
        self.__setup_scrollbars__()
        
        self.context_button = tk.Label(self.f_packages, text="c", borderwidth=2, relief="groove")
        self.context_button.bind("<Button-1>", self.__contextmenu_open__)
        self.context_button.grid(row=1, column=1)
        
        # Treeview has the highest priority when it comes to size
        self.f_packages.rowconfigure(0, weight=1)
        self.f_packages.columnconfigure(0, weight=1)
        
        self.loading_progress = tk.IntVar()
        self.pb_loading = ttk.Progressbar(self, variable=self.loading_progress)
        self.show_pb_loading = True
        self.loading_progress.set(25)
        self.pb_loading.grid(row=1, column=0, columnspan=2, sticky="we")
        
        #self.th = RosPackageView.PackageRetrieval(None)
        #self.th.start()
        
        #self.loading_thread = Thread(target=self.populate)
        #button = ttk.Button(self, text="Load packages", command=lambda: self.loading_thread.start())
        #button.grid()
                
    def __setup_treeview__(self):
        """
        Creates the treeview where the packages will be displayed and can be accessed
        """
        self.tv_packages_header = ["Package", "Location", "Dependencies", "Status"]
        self.tv_packages = ttk.Treeview(master=self.f_packages, columns=self.tv_packages_header, show="headings")
        
        self.tv_packages.bind("<Button-3>", self.__contextmenu_open__)
        
        # Center alignment for status column
        self.tv_packages.column("Status", anchor="center")
        
        for col in self.tv_packages_header:
            self.tv_packages.heading(col, text=col.title(), command=partial(self.__treeview_sort_column__, col, reverse=False))
            
        # Row colouring - even/odd alternation of colour, special handling of row that have error as status
        self.tv_packages.tag_configure("oddrow", background="light blue")
        self.tv_packages.tag_configure("evenrow", background="powder blue")
        self.tv_packages.tag_configure("missingdeps", background="tomato")
        
    def __setup_scrollbars__(self):
        """
        Creates a scrollbar that is attached to the treeview
        """
        self.scrollbar_x = ttk.Scrollbar(self.f_packages, orient=tk.HORIZONTAL)
        self.scrollbar_x.grid(row=1, column=0, sticky="we")
        self.scrollbar_x.config(command=self.tv_packages.xview)
        self.tv_packages.config(xscrollcommand=self.scrollbar_x)
        
        self.scrollbar_y = ttk.Scrollbar(self.f_packages)
        self.scrollbar_y.grid(row=0, column=1, sticky="ns")
        self.scrollbar_y.config(command=self.tv_packages.yview)
        self.tv_packages.config(yscrollcommand=self.scrollbar_y)
        
    def __toggle_progressbar__(self):
        if self.show_pb_loading:
            self.pb_loading.grid_forget()
        else:
            self.pb_loading.grid(row=1, column=0, columnspan=2, sticky="we")
        
        self.show_pb_loading = not self.show_pb_loading
        
    def __setup_contextmenu_(self):
        """
        Creates the context menu (with all its submenus) for the treeview
        """
        
        # Context menu
        self.cm_packages = tk.Menu()
        self.cm_packages_options = ["Fix dependencies", "Update", "Uninstall"]
        # Context submenu for "Copy..."
        # Allows copying a selection of items to clipboard in CSV or JSON format
        self.cm_sub_copy = tk.Menu(self.cm_packages)
        self.cm_sub_copy_options = ["CSV", "JSON"]
        # Context submenu for "Select..."
        # Allows selecting a range (through a popup dialog), all or none items
        self.cm_sub_selection = tk.Menu(self.cm_packages)
        self.cm_sub_selection_options = ["Range", "All", "None"]
        
        # Context submenu for "Sort.."
        self.cm_sub_sort = tk.Menu(self.cm_packages)
        self.cm_sub_sort_options = ["Package", "Location", "Status"]
          
        item_id = 0
        for item in self.cm_packages_options:
            self.cm_packages.add_command(label=item, command=partial(self.__contextmenu_selected__, item_id))
            item_id += 1
        self.cm_packages.entryconfig("Fix dependencies", state="disabled")
        
        item_id = 0
        for item in self.cm_sub_copy_options:
            self.cm_sub_copy.add_command(label=item, command=partial(self.__sub_contextmenu_copy_selected__, item_id))
            item_id += 1
        self.cm_packages.add_cascade(label="Copy...", menu=self.cm_sub_copy)
            
        item_id = 0
        for item in self.cm_sub_selection_options:
            self.cm_sub_selection.add_command(label=item, command=partial(self.__sub_contextmenu_selection_selected__, item_id))
            item_id += 1
        self.cm_packages.add_cascade(label="Select...", menu=self.cm_sub_selection)
        
    def __contextmenu_open__(self, event):
        """
        Handles opening the context menu
        @param event Right button click over row that is in a selected state
        """
        ros_package = self.tv_packages.identify_row(event.y)
        #if not ros_package: return
        
        curr_selected_packages = self.tv_packages.selection()
        if not curr_selected_packages:
            self.cm_packages.entryconfig("Copy...", state="disabled")
            self.cm_packages.entryconfig("Update", state="disabled")
            self.cm_packages.entryconfig("Uninstall", state="disabled")
        else:
            self.cm_packages.entryconfig("Copy...", state="normal")
            self.cm_packages.entryconfig("Update", state="normal")
            self.cm_packages.entryconfig("Uninstall", state="normal")
                    
        found_error = False
        for selected_package in curr_selected_packages:
            if str(self.tv_packages.item(selected_package)["values"][3]) == "Failed":
                self.cm_packages.entryconfig("Fix dependencies", state="normal")
                found_error = True
                
        if not found_error:
            self.cm_packages.entryconfig("Fix dependencies", state="disabled")
            
        self.cm_packages.post(event.x_root, event.y_root)
        
    def __sub_contextmenu_copy_selected__(self, item):
        """
        Handler for the "Copy..." context submenu. Copies the selection to clipboard
        either as CSV or JSON. Both can be used to integrate the package data in other
        applications, spreadsheets etc.
        @param item Copy mode based on which entry in the context submenu was selected
        """
        data = ""
        curr_selected_packages = self.tv_packages.selection()
        
        if item == 0:
            # CSV
            print("Copying to clipboard as CSV")
            csv_data = ""
            
            def format_for_csv(pkg_prop):
                if pkg_prop == "Failed":
                    return str(1)
                elif pkg_prop == "Ok":
                    return str(0)          
                else:
                    return pkg_prop
            
            for ros_package in curr_selected_packages:
                csv_data += ";".join([str(format_for_csv(pkg_prop))
                                      for pkg_prop in self.tv_packages.item(ros_package)["values"]]) + "\n"
                
            data = csv_data[:-1]
        elif item == 1:
            print("Copying to clipboard as JSON")
            json_data = ""
            
            def format_to_json(ros_package):
                """
                Create JSON object from the given package and return it as string
                """
                json_obj = {}
                for (key, value) in zip(self.tv_packages_header, ros_package):
                    if value == "Failed":
                        json_obj[key.lower().replace(" ", "_")] = 1
                    elif value == "Ok":
                        json_obj[key.lower().replace(" ", "_")] = 0
                    elif value == "":
                        pass
                    else:
                        json_obj[key.lower().replace(" ", "_")] = value
                    
                return dumps(json_obj)
            
            for ros_package in curr_selected_packages:
                json_data += format_to_json(self.tv_packages.item(ros_package)["values"]) + ",\n"
                
            data = json_data[:-2]
            
        # https://stackoverflow.com/a/4203897/1559401
        # Copy CSV/JSON output to clipboard
        root = tk.Tk()
        root.withdraw()
        root.clipboard_clear()
        root.clipboard_append(data)
        root.update() # Keep clipboard content alive even if application is closed
        
    def __treeview_sort_column__(self, col, reverse):
        """
        Handler for a column heading. It allows sorting decending/ascending of the
        whole treeview based on order of the values in the given column
        """
        l = [(self.tv_packages.set(k, col), k) for k in self.tv_packages.get_children("")]
        l.sort(reverse=reverse)
    
        # rearrange items in sorted positions
        for index, (val, k) in enumerate(l):
            self.tv_packages.move(k, "", index)
    
        # reverse sort next time
        print(col)
        self.tv_packages.heading(col, command=lambda: \
                   self.__treeview_sort_column__(col, not reverse))
    
    def __sub_contextmenu_selection_selected__(self, item):
        if item == 0:
            print("Select range")
        elif item == 1:
            for ros_package in self.tv_packages.get_children():
                self.tv_packages.selection_add(ros_package)
        elif item == 2:
            for ros_package in self.tv_packages.get_children():
                self.tv_packages.selection_remove(ros_package)
            
    def __contextmenu_selected__(self, item):
        print(item)
        # Get currently selected packages to apply the respective action from the context menu on those
        curr_selected_packages = self.tv_packages.selection()
        print(curr_selected_packages)
        
        if item == 0:
            print("Attempting to fix issues")
        
        self.tv_packages.selection_toggle(self.tv_packages.selection())
        
    def __fix__(self, ros_package_names):
        pass
            
    def populate(self, filter="none"):
        if self.tv_packages.get_children():
            for ros_package in self.tv_packages.get_children():
                self.tv_packages.delete(ros_package)
            
        ros_packages_install_retrieve = subprocess.Popen(["rospack", "list"], shell=False, stdout=subprocess.PIPE)
        
        #self.pb_loading["maximum"] = len(ros_packages_install_retrieve.stdout.readlines())
        
        ros_packages_installed = []
        for ros_package_raw in ros_packages_install_retrieve.stdout.readlines():
            ros_package = ros_package_raw.split(" ")
            ros_package[1] = ros_package[1][:-1]
            ros_packages_installed.append(ros_package)
            print("Found package " + ros_package[0] + " in " + ros_package[1])
            
        ros_packages_deps = []
        i = 0
        for ros_package in ros_packages_installed[-7:]: #[-7:]
            deps_and_status = self.__get_package_deps__(ros_package[0])
            ros_packages_deps.append(deps_and_status)
            
        ros_packages_full = zip([ros_package[0] for ros_package in ros_packages_installed],
                                [ros_destination[1] for ros_destination in ros_packages_installed],
                                [", ".join(rpd[0]) for rpd in ros_packages_deps],
                                [rpd[1] for rpd in ros_packages_deps])
        
        even = True
        for package in ros_packages_full[-7:]: #[-7:]
            pkg = (package[0], package[1], package[2], "Ok" if package[3] == 0 else "Failed")
            if package[3] != 0:
                self.tv_packages.insert("", "end", values=pkg, tags = ("missingdeps",))
            else:
                self.tv_packages.insert("", "end", values=pkg, tags = ("evenrow",) if even else ("oddrow",))
            even = not even
            
    def __get_package_deps__(self, ros_package):
        ros_package_deps_retrieve = subprocess.Popen(["rospack", "depends", ros_package], shell=False, stdout=subprocess.PIPE)
        ros_package_deps = []
        print("Retrieving dependencies for package " + ros_package)
        for ros_package_dep in ros_package_deps_retrieve.stdout.readlines():
            ros_package_dep = ros_package_dep.split("\n")
            ros_package_dep = ros_package_dep[:-1]
            ros_package_deps += ros_package_dep
            
        ros_package_deps_retrieve.wait()
        print(("Package " + ros_package + " depends on " + ", ".join(ros_dep for ros_dep in ros_package_deps)) if ros_package_deps != [] else ("No dependencies found for package " + ros_package))
        
        return (ros_package_deps, ros_package_deps_retrieve.returncode) # TODO Add "Fix me" context menu to tree view that will allow handling issues with the dependecies retrieval (for example if missing deps then offer to automatically download and install those)

    
main_window = tk.Tk()
main_window.columnconfigure(0, weight=1)
main_window.rowconfigure(0, weight=1)

view = RosPackageView(main_window)
view.grid(sticky="nwes")
view.columnconfigure(0, weight=1)
view.rowconfigure(0, weight=1)
view.populate()

main_window.mainloop()