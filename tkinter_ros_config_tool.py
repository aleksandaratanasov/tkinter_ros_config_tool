#!/usr/bin/env python2.7
'''
Created on Aug 13, 2017

@author: Aleksandar Atanasov, redbaronuqueen@gmail.com
'''

import Tkinter as tk
import ScrolledText
import ttk
import sys


class TRWorkspaceSetupDialog(object):
    def __init__(self, master, current_workspace_path):
        top = self.top = tk.Toplevel(master)
        top.wm_title("Setup catkin workspace")
        
        self.path = current_workspace_path
        
        f_content = ttk.Frame(self.top, padding=(3,3,12,12))
        l_path = ttk.Label(f_content, text="Absolute path to catkin workspace")
        self.e_path = ttk.Entry(f_content)
        self.e_path.insert(tk.END, self.path if self.path else "")
        self.__select_path_string__()
        b_browse = ttk.Button(f_content, text="Browse", command=self.__browse__)
        b_browse.focus()
        b_accept = ttk.Button(f_content, text="Okay", command=self.__submit__)
        b_reject = ttk.Button(f_content, text="Cancel", command=self.__reject__)
        
        f_content.grid(column=0, row=0, sticky=(tk.N, tk.S, tk.E, tk.W))
        l_path.grid(column=0, row=0, columnspan=2, sticky=(tk.N, tk.W), padx=5)
        self.e_path.grid(column=0, row=1, columnspan=3, sticky=(tk.N, tk.E, tk.W), pady=5, padx=5)
        b_browse.grid(column=0, row=3)
        b_accept.grid(column=1, row=3)
        b_reject.grid(column=2, row=3)
        
        self.top.columnconfigure(0, weight=1)
        self.top.rowconfigure(0, weight=1)
        f_content.columnconfigure(0, weight=3)
        f_content.columnconfigure(1, weight=3)
        f_content.columnconfigure(2, weight=3)
        f_content.rowconfigure(1, weight=1)
        
    def __select_path_string__(self):
        self.e_path.select_range(0, tk.END)
        self.e_path.icursor(0)
        
    def __browse__(self):
        from tkFileDialog import askdirectory
        path = askdirectory()
        if path:
            self.e_path.delete(0, tk.END)
            self.e_path.insert(0, path)
        
    def __reject__(self):
        self.path = None
        self.top.destroy()   
             
    def __submit__(self):
        path = self.e_path.get()
        if self.__check_path__(path):
            self.path = path
            self.top.destroy()
        else:
            print("Given path is not a valid catkin workspace")
            self.e_path.focus()
            self.__select_path_string__()
        
    def __check_path__(self, path):
        from os import listdir
        
        if not path: return False
        
        files = listdir(path)
        is_catkin_workspace= False
        for file in files:
            if file == ".catkin_workspace":
                is_catkin_workspace = True
                break
            
        if is_catkin_workspace:
            return True
        
        return False
    
# TODO No need for a separate message class if treeview is used.
class TRLogMessage:
    """
    Represents a log message for the TRLogger
    """
    def __init__(self, id, message, type, source):
        """
        It contains following data:
        @param id Incremental and is used for fast access to the given message. It is viewed as a dictionary key by the TRLogger
        @param message The message that is logged
        @param type Type of the log message. Can be info, error or warning
        @param source The name of the component that triggered the log message
        """
        from time import time
        self.timestamp = int(time())
        self.id = id
        self.message = message
        self.type = type
        self.source = source
    
    
class TRLogger:
    """
    Used for adding logging capabilities to the application.
    Each log message has the following structure:
    
    [<id>] <prefix> <message> <source>
    
    where
      * id is a counter that is incremented every time a log
        message has been logged. It can be used for example when
        using the copy entry to specify single, a set or a range
        of log messages to be copied to clipboard
        
      * prefix specifies if the log message is simply giving
        information, represents an error or a warning
      * message is the actual log message that will be logged
      * 
    """
    
    # Source
    class TRLogSource:
        ROS = 1
        GUI = 2
        TOOL = 3
    
    # Type
    class TRLogType:
        INFO = 1
        ERROR = 2
        WARN = 3
        
    # Mode
    class TRLogOutputMode:
        CONSOLE = 0
        GUI = 1
        FILE = 2
    
    def __init__(self):
        self.enable = True
        self.log_counter = 0
        self.logged_messages = {}
        
    def toggle_logger(self, enable=True):
        self.enable = enable
    
    def log_info(self, msg, mode, source, target):
        self.log(msg, TRLogger.TRLogType.INFO, mode, source, target)
      
    def log_error(self, msg, mode, source, target):
        self.log(msg, TRLogger.TRLogType.ERROR, mode, source, target)
 
    def log_warn(self, msg, mode, source, target):
        self.log(msg, TRLogger.TRLogType.WARN, mode, source, target)
        
    def log_console(self, full_msg):
        """
        Output full message (prefix + msg + source) to console
        """
        if full_msg:
            print(full_msg)
    
    def log_gui(self, full_msg, gui_console):
        """
        Output full message (prefix + msg + source) to TRLogConsole
        @param gui_console A valid instance of TRLogConsole
        """
        pass
    
    def log_file(self, full_msg, file):
        """
        Output full message (prefix + msg + source) to file
        @param file A valid text file. If it does not exist, a new one will be created. If no write access possible, no log message will be written
        """
        pass
        
    def log(self, msg, type, mode, source, target):
        """
        @param msg Message that will be logged
        @param type Type of the message (info, error or warn)
        @param mode Log output (console, GUI or file)
        @param source What generated the error message (ROS, GUI or tool itself). If source is GUI, name of the GUI element will be called)
        @param target If mode is set to
                       * GUI, target needs to be an instance of TRLogConsole
                       * file, target needs to be a valid text file. If not such file exists an attempt will be made to create it. If no write access is grandet, logging will fail
        """
        # Prevent logging if enable is False
        if not self.enable:
            return
        
        def get_log_prefix(type):
            """
            Return prefix that can be used to specify the type of the logged msg
            @param type A valid TRLogger.TRLogType. If invalid, None will be returned
            """
            types = [TRLogger.TRLogType.INFO, TRLogger.TRLogType.ERROR, TRLogger.TRLogType.WARN]
            if type in types:
                prefixes = zip(types, ["INFO", "ERROR", "WARN"])
                return prefixes[type][1]
            else:
                return None
            
        self.log_counter += 1
                  
        prefix = None
        if mode == TRLogger.TRLogOutputMode.CONSOLE:
            prefix = get_log_prefix(type)
            if prefix:
                print("[" + str(self.log_counter) + "]" + prefix + "\t" + source.name)
        elif mode == TRLogger.TRLogOutputMode.GUI and isinstance(target, TRLogConsole):
            self.mode = mode
            self.target = target
        else:
            self.mode = TRLogger.TRLogOutputMode.CONSOLE
            self.target = None
            print("Unknown logger mode. Setting to default \"CONSOLE\"")    

# TODO Replace Text box with a table to enable sorting log messages by type, id or timestamp
class TRLogConsole(ttk.Frame):
    def __init__(self, master, logger=None):
        ttk.Frame.__init__(self, master)
        
        self.logger = TRLogger() if not logger else logger
        
        # Text widget with scrollbar for displaying log messages
        text_box = ttk.Frame(self)
        
        scrollbar = ttk.Scrollbar(text_box)
        scrollbar.grid(row=0, column=0, sticky=tk.N+tk.S)
        #textbox = tk.Text(text_box)
        
        
        ####################################################################################
        ####################################################################################
        ####################################################################################
        from random import choice
        from string import digits, ascii_lowercase
        import datetime
        import time
        chars = digits + ascii_lowercase
        lb_header = ["id", "type", "timestamp", "message", "source"]
        types = ["INFO", "WARN", "ERROR"]
        sources = ["ROS", "GUI"]
        timestamps = []
        tstamp = time.time()
        for i in range(100):
            timestamps.append(int(tstamp))
            tstamp += 1
        lb_list = zip(range(100),
                      [choice(types) for i in range(100)],
                      [datetime.datetime.fromtimestamp(tstamp).strftime('%Y-%m-%d %H:%M:%S') for tstamp in timestamps],
                      ["".join([choice(chars) for i in range(20)]) for j in range(100)],
                      [choice(sources) for i in range(100)])
        textbox = ttk.Treeview(master=text_box, columns=lb_header, show="headings")
        textbox.tag_configure("error", background="tomato")
        textbox.tag_configure("info", background="white")
        textbox.tag_configure("warn", background="khaki1")
        for col in lb_header:
            textbox.heading(col, text=col.title())
            
        for item in lb_list:
            if item[1] == "INFO":
                textbox.insert("", "end", values=item, tags = ('info',))
            elif item[1] == "WARN":
                textbox.insert("", "end", values=item, tags = ('warn',))
            elif item[1] == "ERROR":
                textbox.insert("", "end", values=item, tags = ('error',))
        ####################################################################################
        ####################################################################################
        ####################################################################################    
            
        
        textbox.grid(row=0, column=1, sticky=tk.N+tk.S)
        scrollbar.config(command=textbox.yview)
        textbox.config(yscrollcommand=scrollbar.set)
        
        text_box.grid(row=0, columnspan=2)
        
        # Checkbuttons for filtering log messages
        mode_controls = ttk.Frame(self)

        self.v_info = tk.BooleanVar()
        self.v_info.set(True)
        self.c_info = ttk.Checkbutton(mode_controls, text="Info", variable=self.v_info, onvalue=True, command=self.__toggle_info__)
        
        self.v_error = tk.BooleanVar()
        self.v_error.set(True)
        self.c_error = ttk.Checkbutton(mode_controls, text="Error", variable=self.v_error, onvalue=True, command=self.__toggle_error__)
        
        self.v_warning = tk.BooleanVar()
        self.v_warning.set(False)
        self.c_warning = ttk.Checkbutton(mode_controls, text="Warning", variable=self.v_warning, onvalue=True, command=self.__toggle_warning__)
        
        self.v_all = tk.BooleanVar()
        self.v_all.set(False)
        self.c_all = ttk.Checkbutton(mode_controls, text="All", variable=self.v_all, onvalue=True, command=self.__toggle_all__)
        
        self.c_info.grid(column=0, row=0)
        self.c_error.grid(column=1, row=0)
        self.c_warning.grid(column=2, row=0)
        self.c_all.grid(column=3, row=0)
        
        mode_controls.grid(column=0, row=1, sticky=tk.W)
        
        # Misc buttons - clear, manage output files, turn on/off logging, switch between outputs (console, textbox, file)
        misc_controls = ttk.Frame(self)
        
        self.v_turn_on_off = tk.BooleanVar()
        self.v_turn_on_off.set(True)
        c_turn_on_off = ttk.Checkbutton(misc_controls, text="Enable logging", variable=self.v_turn_on_off, onvalue=True, command=self.__toggle_logging__)
        
        c_copy = ttk.Button(misc_controls, text="Copy", command=self.__copy_to_clipboard__)
        
        # TODO The use can enter a single log message id, 
        e_copy = ttk.Entry(misc_controls)
        
        c_turn_on_off.grid(column=0, row=0)
        e_copy.grid(column=1, row=0)
        c_copy.grid(column=2, row=0)
        
        misc_controls.grid(column=1, row=1, sticky=tk.W)
        
    def log_info(self, msg, source):
        if self.logger:
            # TODO Get mode from combobox
            pass
        
    def log_error(self, msg, source):
        if self.logger:
            pass
        
    def log_warn(self, msg, source):
        if self.logger:
            pass
        
    def __toggle_all_auto__(self):
        self.v_all.set(self.v_info.get() and self.v_error.get() and self.v_warning.get())
        
    def __toggle_all_manual__(self):
        self.v_info.set(self.v_all.get())
        self.v_error.set(self.v_all.get())
        self.v_warning.set(self.v_all.get())
        
        
    def __toggle_info__(self):
        # TODO output state to console, textbox or file
        self.__toggle_all_auto__()
        
    def __toggle_error__(self):
        # TODO output state to console, textbox or file
        self.__toggle_all_auto__()
        
    def __toggle_warning__(self):
        # TODO output state to console, textbox or file
        self.__toggle_all_auto__()
        
    def __toggle_all__(self):
        # TODO output state to console, textbox or file
        self.__toggle_all_manual__()
        
    def __toggle_logging__(self):
        # TODO output state to console, textbox or file
        if self.logger:
            toggle = self.v_turn_on_off.get()
            self.logger.enable = toggle
            self.c_info.configure(state="normal" if toggle else "disabled")
            self.c_error.configure(state="normal" if toggle else "disabled")
            self.c_warning.configure(state="normal" if toggle else "disabled")
            self.c_all.configure(state="normal" if toggle else "disabled")
            
    def __copy_to_clipboard__(self):
        """
        Copies to clipboad entire log or a range of log messages
        """
        pass
            
class TRInstallationManager(ttk.Frame):
    """
    Manages multiple ROS installations by allowing the user to point at the ROS root folder (usually /opt/ros/<ros-release>)
    """
    
    def __init__(self, master):
        pass
    
class TRWorkspaceManager(ttk.Frame):
    """
    Manages multiple ROS catkin workspaces. rosbuild 
    """
    
    def __init__(self, master):
        pass
            

class TRConfigurationTool(ttk.Frame):
    
    def __init__(self, master):
        ttk.Frame.__init__(self, master)
        self.master = master
        
        self.current_workspace_path = None
        
        content = ttk.Frame(master)
        
        log_console = TRLogConsole(content)
        
        namelbl = ttk.Label(content, text="Name")
        name = ttk.Entry(content)
        ok = ttk.Button(content, text="Okay")
        cancel = ttk.Button(content, text="Cancel")
        
        content.grid(column=0, row=0, sticky=(tk.N, tk.S, tk.E, tk.W))
        
        log_console.grid(column=0, row=0, columnspan=3, rowspan=2, sticky=(tk.N, tk.S, tk.E, tk.W))
        #c_info.grid(column=0, row=3)
        #c_error.grid(column=1, row=3)
        #c_warning.grid(column=2, row=3)
        
        namelbl.grid(column=3, row=0, columnspan=2, sticky=(tk.N, tk.W), padx=5)
        name.grid(column=3, row=1, columnspan=2, sticky=(tk.N, tk.E, tk.W), pady=5, padx=5)
        ok.grid(column=3, row=3)
        cancel.grid(column=4, row=3)
        
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        content.columnconfigure(0, weight=3)
        content.columnconfigure(1, weight=3)
        content.columnconfigure(2, weight=3)
        content.columnconfigure(3, weight=1)
        content.columnconfigure(4, weight=1)
        content.rowconfigure(1, weight=1)
        
        self.b_workspace = ttk.Button(master,text="click me!",command=self.__popup__)
        self.b_workspace.grid()
        
    def __popup__(self):
        self.w = TRWorkspaceSetupDialog(self.master, self.current_workspace_path)
        self.__enable_all_(False)
        self.master.wait_window(self.w.top)
        self.__enable_all_(True)
        new_workspace_path = self.__workspace_path__()
        self.current_workspace_path = new_workspace_path if new_workspace_path != self.current_workspace_path else self.current_workspace_path
        
    def __workspace_path__(self):
        return self.w.path
    
    def __enable_all_(self, enable=True):                
        self.b_workspace["state"] = "normal" if enable else "disable"
            

def main():
    print("Tkinter ROS Configuration Tool\nby Aleksandar Atanasov")
    
    from os import environ
    try:
        ros_pkg_path = environ["ROS_PACKAGE_PATH"]
    except KeyError:
        print("""
        Variable ROS_PACKAGE_PATH seems to be empty or non-existent.
        Please configure your environment before using this tool.
        More information can be found in the official ROS wiki at
        http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment
        """)
        return -1
    
    # Find catkin workspace using ROS_PACKAGE_PATH
    # If not found start setup to allow user to set the path to the workspace manually
    root = tk.Tk()
    root.wm_title("Tkinter ROS Configuration Tool")
    m = TRConfigurationTool(root)
    root.mainloop()
        
    #app = TkinterRosConfigurationTool()
    #app.mainloop()

if __name__ == '__main__':
    main()